from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, ItemForm
# Create your views here.

def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "list": list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    lists = TodoList.objects.get(id=id)
    context = {
        "lists": lists,
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_list_createitem(request):
    items = TodoList.objects.all()
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
        "items": items,
    }
    return render(request, "todos/createitem.html", context)

def todo_list_updateitem(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = ItemForm(instance=item)
    context = {
        'form': form,
    }
    return render(request, 'todos/edititem.html', context)
